# Handles logging operations for the app.
from enum import Enum


class PriorityElement():
	'''Create a new PriorityElement.

	Parameters:
		* value: The numeric priority as an integer. The lower the value,
		the higher-priority it is.
		* color: The RGB color of the element, as TBD.
	'''
	def __init__(self, value, color):
		self.__value = value
		self.__color = color

	def value(self):
		return self.__value

	def color(self):
		return self.__color

'''Describes what priority and color a given log message is.
The lower the value, the higher-priority it is.
'''
class Priority(Enum):
	ERROR = PriorityElement(0, None)
	WARN = PriorityElement(1, None)
	PASS = PriorityElement(2, None)
	START = PriorityElement(3, None)
	LOG = PriorityElement(4, None)
	DEBUG = PriorityElement(5, None)

'''Handles logging operations for the app.
'''
class Logger:
	'''Creates a new Logger instance.

	Parameters:
		* verbose: if True, the logger will record debug messages.
	'''
	def __init__(self, verbose):
		self.__verbose = verbose

	'''Logs an error to the logger.
	'''
	def e(self, log_text):
		self.__log(log_text, Priority.ERROR)

	'''Logs a warning to the logger.
	'''
	def w(self, log_text):
		self.__log(log_text, Priority.WARN)

	'''Logs a success message ("pass") to the logger.
	Uses "p" since "s" is taken for start messages.
	'''
	def p(self, log_text):
		self.__log(log_text, Priority.PASS)

	'''Logs a start message to the logger.
	'''
	def s(self, log_text):
		self.__log(log_text, Priority.START)

	'''Logs a normal message to the logger.
	'''
	def l(self, log_text):
		self.__log(log_text, Priority.LOG)

	'''Logs a debug message to the logger.
	These will not be displayed if verbose mode is disabled.
	'''
	def d(self, log_text):
		if self.__verbose:
			self.__log(log_text, Priority.DEBUG)

	'''Performs the actual logging operation.

	Parameters:
		* log_text: The text to record to the output log.
		* priority: The PriorityElement representing the message's priority.
	'''
	def __log(self, log_text, priority):
		raise NotImplementedError()
