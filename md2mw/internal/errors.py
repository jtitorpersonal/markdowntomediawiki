# Defines errors used by the app.

class ConversionFailedError(RuntimeError):
	'''Creates a new ConversionFailedError.
	
	Parameters:
		* reason: The reason this error occured.
	'''
	def __init__(self, reason):
		super().__init__(reason)
