# Handles conversion between Markdown and MediaWiki for the app.

'''Handles conversion between Markdown and MediaWiki for the app.
'''
class Converter:
	'''Performs conversion in the given root directory,
	using the given logger.

	Parameters:
		* root_directory: The path to the directory whose files and recursive
		subdirectories will be converted.
		* logger: The Logger instance the Converter will use to log its
		operations.
	Returns:
		* Nothing.
	Throws:
		* TBD.
	'''
	def run(self, root_directory, logger):
		validated_params = self.__validate(root_directory, logger)
		root_directory = validated_params[0]
		logger = validated_params[1]

		self.__convert_dir(root_directory, logger)

	'''Validates the parameters and converts them if needed.

	Parameters:
		* root_directory: The path to the directory whose files and recursive
		subdirectories will be converted.
		* logger: The Logger instance the Converter will use to log its
		operations.
	Returns:
		* An array containing the following:
			* 0: root_directory converted to a Path
			* 1: logger, unconverted.
	Throws:
		* TBD if either root_directory or logger aren't valid for use in
		the converter.
	'''
	def __validate(self, root_directory, logger):
		if not logger:
			raise RuntimeError("No valid Logger, can't convert!")
		root_as_path = self.__validate_root(root_directory)

		return [root_as_path, logger]

	'''Validates and converts the root directory parameter.

	Parameters:
		* root_directory: The path to the directory whose files and recursive
		subdirectories will be converted.
	Returns:
		* root_directory, converted to a Path.
	Throws:
		* TBD if root_directory is not a valid directory path. It is invalid
		if any of the following are true:
			* The string cannot create a Path object
			* The path represented by the Path object does not exist
			* The path represented by the Path object cannot be
			accessed (permissions, network error, etc.)
	'''
	def __validate_root(self, root_directory):
		pass

	'''Converts the given directory and its recursive subdirectories.

	Parameters:
		* dir_path: The path to the directory tree root to convert,
		as a Path object.
	Returns:
		* Nothing.
	Throws:
		* TBD if not all files or subdirectories could be converted.
	'''
	def __convert_dir(self, dir_path):
		file_paths = self.__get_files(dir_path)
		subdir_paths = self.__get_subdirs(dir_path)

		for f in file_paths:
			self.__convert_file(f)

		for s in subdir_paths:
			self.__convert_dir(s)

	'''Converts the given Markdown file to MediaWiki format.

	Parameters:
		* file: The path to the file to convert, as a Path object.
	Returns:
		* Nothing.
	Throws:
		* TBD
	'''
	def __convert_file(self, file_path):
		pass

	'''Gets a list of all Markdown (.md) files in the given directory.

	Parameters:
		* dir_path: The path to the directory tree root to convert,
		as a Path object.
	Returns:
		* A list of all Markdown files in dir_path, as Path objects.
	Throws:
		* TBD
	'''
	def __get_files(self, dir_path):
		# We can only determine the format by file extension!
		# For each file in the directory:
		#	* Add it to the list if the file has extension ".md".
		pass

	'''Gets a list of all subdirectories of the given directory.

	Parameters:
		* dir_path: The path to the directory tree root to convert,
		as a Path object.
	Returns:
		* A list of all subdirectories of dir_path, as Path objects.
	Throws:
		* TBD
	'''
	def __get_subdirs(self, dir_path):
		pass