# Handles argument-parsing for the app.

class ParsedArgs:
	def __init__(self, verbose, root_dir):
		self.verbose = verbose
		self.root_dir = root_dir

'''Handles argument-parsing for the app.
'''
class Args:
	'''Parses the invocation into app-usable parameters.

	Returns:
		* A dictionary of parameters retrieved from the invocation line. Structure TBD.
	Throws:
		* Anything that ArgumentParser throws.
	'''
	def parse(self):
		parser = self.__create_parser()
		args = parser.parse_args()
		return self.__convert_args(args)

	'''Creates the ArgumentParser used by parse().

	Returns:
		* An ArgumentParser that is fully configured for the invocation line. Run it with .parse_args().
	'''
	def __create_parser(self):
		pass

	'''Converts the given arguments into an app-usable parameter dictionary.

	Returns:
		* A dictionary of parameters retrieved from the invocation line. Structure TBD.
	'''
	def __convert_args(self, args):
		pass