#! /usr/bin/python3
from internal import *
from internal.errors import *

'''Converts Markdown files to Mediawiki files.
'''
def __main__():
	try:
		__run()
	except Exception as e:
		__handle_logger_init_failed(e)

'''TODO
'''
def __run():
	components = __create_components()
	__perform_conversion(components[0], components[1])

'''TODO
'''
def __create_components():
	arg_parser = args.Args()
	parameters = arg_parser.parse()
	logger = log.Logger(parameters.verbose)

	return [parameters, logger]

'''TODO
'''
def __perform_conversion(parameters, logger):
	try:
		convert.Converter().run(parameters.root_dir, logger)
	except ConversionFailedError as e:
		__handle_conversion_failed(e, logger, parameters)
	except Exception as e:
		__handle_converter_crashed(e, logger, parameters)

# Error handlers...
'''TODO
'''
def __handle_logger_init_failed(error):
	print("Couldn't create logger; can't continue with conversion!")
	print("Reason: '{0}'".format(error))
	exit(1)

'''TODO
'''
def __handle_conversion_failed(error, logger, parameters):
	logger.e("Conversion of directory '{0}' failed!".format(parameters.root_dir))
	logger.e("Reason: '{0}'".format(error))
	exit(1)

'''TODO
'''
def __handle_converter_crashed(error, logger, parameters):
	logger.e("Converter crashed; cannot convert directory '{0}'!".format(parameters.root_dir))
	logger.e("Reason: '{0}'".format(e))
	exit(1)